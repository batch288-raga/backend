// Use the "require" directove to load Node.js modules
// A module is a software component or party of a program contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// In that way, HTTP is a protocol that allows the fetching of resources such as HTML documents

// Clients (browser) and Servers (NodeJS/Express JS Applications communicate by exchanging individual messages).
// These messages are sent by the clients, usually a web browser and aclled "request"

// The messages sent by the server as an answer called "response"
let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to request on a specified port and gives responses back to the client
// A port is a virtual point where network connection starts and end.
// The http module has a createServer() method that accepts a function as an argument and allows for creation of a server
// The arguments passed in the feunction are request and response objects (data types) that contains methods that allows us to receive request from the client and send responses back to it

// The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our server
http.createServer(function (request, response) {

	// Use the writeHead() method to:
	// Set a status code for the response - a 200 means OK
	// Set the content-type of the response as a plain text messages
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// Send the response with the text content 'Hellow World?!'
	response.end('Hellow World?!');

}).listen(8000);

// When the server is running, console will print the message:
console.log('Server running at localhost:8000');