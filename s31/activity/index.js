// 1. What directive is used by Node.js in loading the modules it needs?

	// Answer: Require directive is used to load a Node module(http) and store returned its instance(http) into its variable(http).

// 2. What Node.js module contains a method for server creation?

	// Answer: The http module contains the function to create the server, which we will see later on. If you would like to learn more about modules in Node.

// 3. What is the method of the http object responsible for creating a server using Node.js?

	// Answer: The http. createServer() method creates an HTTP Server object. The HTTP Server object can listen to ports on your computer and execute a function, a requestListener, each time a request is made.

// 4. What method of the response object allows us to set status codes and content types?
	
	// Answer: You simply call the response objects status method. res. status(404). send("Not Found");

// 5. Where will console.log() output its contents when run in Node.js?

	// Answer: The console. log() method outputs a message to the web console. The message may be a single string (with optional substitution values), or it may be any one or more JavaScript objects

// 6. What property of the request object contains the address's endpoint?

	// Answer:The req object represents the HTTP request and has properties for the request query string, parameters, body, HTTP headers, and so on.

const http = require('http');

const port = 3000;

const server = http.createServer((request, response) => {
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Welcome to the login page.')
	} else if (request.url == '/register'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end("I'm sorry the page you are looking for cannot be found")
	} else {
		response.writeHead(404, {'Content-Type': 'text-plain'})

		response.end('Hey! Page is not available')
	}
})

server.listen(port)

console.log(`Server is now accessible at localhost:${port}`);