async function findName(db) {
	return await(
		db.users.find(
			{$or:
				[{ firstName: {$regex: "s", $options: "i"}}, {lastName: {$regex: "d", $options: "i"}}]
				},
			{
				firstName: 1,
				lastName: 1,
				_id: 0
			}
			)
		);

};


async function findDeptAge(db) {
	return await (
		db.users.find(
		{$and:
		[{age: {$gte: 70}},{department: "HR"}]

		})
		);

};


async function findNameAge(db) {
	return await (
		db.users.find(
		{$and:
		[{age: {$lte: 30}}, {lastName: {$regex: "e", $options: "i"}}]
		})
		);
};


try{
    module.exports = {
        findName,
        findDeptAge,
        findNameAge
    };
} catch(err){

};