// Advanced Queries

// Query an embedded document / object
db.users.find({
	contact : {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
})

db.users.find({
	contact : {
		email: "stephenhawking@gmail.com"
	}
})

// Dot notation
db.users.find({
	"contact.email" : "stephenhawking@gmail.com"
});

// Querying an array with exact element
db.users.find({
	courses: ["CSS", "JavaScript", "Python"]
});

db.users.find({courses: {
	$all: ["React", "Laravel"]
}})

// Query Operators

// [Section] Comparison Query Operators
	// $gt/gte operator
	/*
		-allows us to find documents that have filed number values greater than or equal to speicified value
		-Note that this operator will only work if the data type of the field is number or integer.
		Syntax:
		db.collectionName.find({ field : {
			$gt: value
		}})
		db.collectionName.find({ field : {
			$gte: value
		}})
	*/

db.users.find({age : {$gt : 76}});

db.users.find({age : {$gte : 76}});
	// $lt/$lte operator
	/*
		-allows us to find documents that have filed number values less than or equal to speicified value
		-Note: same with $gt/$gte operator will only work if the data type of the field is number or integer.
		Syntax:
		db.collectionName.find({field : {$lt: value}})
		db.collectionName.find({field : {$lte: value}})
	*/

db.users.find({age : {$lt : 76}});

db.users.find({age : {$lte : 76}});

	// $ne operator
	/*
		- allows us to find documents that have field number values not equal to specified value.
		Syntax:
		db.collectionName.find({ field: { $ne:value}});
	*/

db.users.find({age : {$ne : 76}});

	// $in operator
	/*
		-allows us to find documents with specific match criteria one field using different values.
		Syntax:
			db.collectionName.find({
				field :
					{
						$in: value
					}
			})
	*/
db.users.find({ lastName: { $in : ["Hawking", "Doe"]}});

db.users.find({lastName: "Hawking"});
db.users.find({lastName: "Doe"})

db.users.find({"contact.phone" : { $in: ["87654321"]}})

// Using in operator in an array

db.users.find({courses: {$in : ["React", "Laravel"]}});

// [SECTION] Logical query operators
	// $or operator
	/*
		- allow us to find documents that match a single criteria from multiple provided search criteria
		Syntax:
			db.collectionName.find({
				$or: [{fieldA: valueA}, {fieldB: valueB}]
			})

	*/

db.users.find({ $or: [{firstName: "Neil"}, {age: 25}]});

// add multiple operators
db.users.find({
	$or: 
	[{firstName: "Neil"}, 
		{age: { $gt: 25}},
		{courses: {$in : ["CSS"]}}
		]
});

	// $and operator
	/*
		-allows us to find documents matching all the multiple criteria in a single field.
		Syntax:
		db.collectionName.find({
			$and:
			[
				{fieldA: valueA},
				{fieldB: valieB},
			]
		})

	*/

db.users.find({
	$and:
	[
		{ age: {$ne: 82}},
		{ age: {$ne: 76}}
		]
});

// Mini-activty
	// You are going to query all the documents from the users collection, that follow these criteria:
		// age must be greater than or equal to 30 that is enrolled in CSS or HTML
// solution:
db.users.find({
	$and: 
	[{age: { $lt: 30}},
	{courses: {$in : ["CSS", "HTML"]}}
	]
});

// [Section] Field Projection
	/*
		-retrieving documents are common operations that we do and by default mongoDB queries return the whole document as a response.
	*/

	// Inclusion
	/*
		-allows us to include or add specific feilds only when retrieving documents:

		Syntax:
		db.collectionName.find({criteria}, {field: 1});

	*/

db.users.find(
	{firstName: "Jane"},
	{
		firstName: 1,
		lastName: 1,
		contact:1,
		_id: 0
	}

)

db.users.find(
	{firstName: "Jane"},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone" :1,
		_id: 0
	}

	)

	// Exclusion
	/*
		allows us to exclude/remove specific fields only when retrieving documents
		Syntax:
			db.users.find({criteria}, {field : 0})	
	*/

db.users.find(
	{firstName : "Jane"},
	{
		contact: 0,
		department: 0
	}
	)

db.users.find(
	{$or:
		[{ firstName: "Jane"}, { age: {$gte: 30}}
			]
		},
	{
		"contact.email": 0,
		department: 0,
		courses: {$slice: 2}
	}
	)

// Evaluation Query Operator
	// $regex operator
		/*
			- allow us to find documents that match a specific string patter using regular expressions.
			-syntax:
			db.users.find({
				field: $regex: 'pattern', option : 'optionValue'
			})
		*/
// Case sensitive
db.users.find({firstName: {$regex: 'n'}});

// Case insensitive
db.users.find({firstName: {$regex: 'n', $options: 'i'}});