// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object


let trainer = {
    name: "Ash Ketchum",
    age: 18,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"],
    },
    talk: function(){
        return "Pikachu! I choose you!";
    }
}

console.log(trainer);
console.log('Result of dot notation:');
console.log(trainer.name);
console.log('Result of square bracket notation');
console.log(trainer['pokemon']);
console.log('Result of talk method');
console.log(trainer.talk());

function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level * 1;
    this.tackle = function(target){
        target.health -= this.attack;
        console.log(this.name + ' tackled ' + target.name);
        console.log(target.name + "'s health is now redeuced to " + target.health);
        if(target.health <= 0){
            this.faint(target)
        }
    };

    this.faint = function(target){
        console.log(target.name + " fainted");
    };
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
// console.log(geodude.name + "'s health is now reduced to " + pikachu.health);
console.log(pikachu);

mewtwo.tackle(geodude);
// console.log(geodude.name + "'s health is now reduced to " + geodude.health);
console.log(geodude);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}