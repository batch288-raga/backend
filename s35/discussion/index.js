const express = require('express');
// Mongoose is a package that allows creation of Schemas to Model our data structures
// also has access to a number of methods for manipulating our database
const mongoose = require('mongoose');


const port = 3001;

const app = express();

	// Section: MongoDB Connection
	// Connect to the database by passing your connection string
	// due to update in Mongo DB drivers that allow connection, the default connection string is being flagged as an error
	// by default a warning will be displayed in the terminal when the application is running.
	// {newUrlParser:true}

	// Syntax:
	// mongoose.connect("MongoDB string", {useNewUrlParser:true});

	mongoose.connect("mongodb+srv://admin:admin@batch288raga.o5oepb3.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true})

	// notification whether we connected properly with the database.

	let db = mongoose.connection;

	// for catching the error just in case we had an error during the connection
	// console.error.bind allows us to print errors in the browser and in the terminal
	db.on("error", console.error.bind(console, "Error! Can't connect to the Database"))

	// if the connection is successful:
	db.once("open", ()=> console.log("We're connected to the cloud database!"))


	// [Section] Mongoose Schemas
	// Schemas determine the structure of the document to be written in the database
	// Schemas act as blueprint to our data
	//Use the schema() constructor of the mongoose module to create a new schema object. 

	// Middlewares
	// allows our app to read json data
	app.use(express.json());
	// allows our app to read data from forms
	app.use(express.urlencoded({extended:true}))

	const taskSchema = new mongoose.Schema({
		// Define the fields with the corresponding data type
		name: String,
		// another field which is status
		status: {
			type: String,
			default: "pending"
		}
	})

	// [Section] Models
	// Uses schema and are used to create/instantiate objects that correspond to the schema.
	// models use schema and they act as middleman from the server(JS code) to our database.

	// to create a model we are going to use the model();

	const Task = mongoose.model('Tasks', taskSchema);

	// [Section] Routes

	// Create a POST route to create a new task
	// Create a new task
	// Business Logic:
		// 1. Add a functionality to check whether there are duplicate tasks
		 	// if the task is existing in the database, we return an error
		 	//if the task doesn't exist in the database, we add it in the database
		//2. The task data will be coming from the request's body

	app.post("/tasks", (request, response) =>{
		// findOne() method is a mongoose method that acts similar to find method in MongoDb.
		// if the findOne finds a document that matches the criteria it will return the object/document and if there's no object that matches the criteria it will return an empty object or null.
		console.log(request.body.name)
		Task.findOne({name: request.body.name})
		.then(result => {
			// we can use if statement to check or verify whether we have an object found
			if(result !== null){
				return response.send("Duplicate task found!");
			}else{

				// Create a new task and save it to the data base.
				let newTask = new Task({
					name: request.body.name
				})

				// The save() method will store the information to the database
				// since the newTask was created from the Mongoose Schema named Task, it will be save in tasks collection
				newTask.save()

				return response.send('New task created')
			}



		})
	})


	// Get all the tasks in our collection
	// 1. Retrieve all the documents
	// 2. If an error is encountered, print the error
	// 3. if no error/s is/are found, send a success status to the client and show the documents retrieved


	app.get("/tasks", (request, response) => {
		// find method is a mongoose method that is similar to MongoDB find
		Task.find({}).then(result => {
			return response.send(result)
		}).catch(error => response.send(error));

	})






if(require.main === module){
	app.listen(port, () => {
		console.log(`Server is running at port ${port}!`)
	})
}

module.exports = app;

const userSchema = new mongoose.Schema({
		username: String,
		password: String
	})

const User = mongoose.model('User', userSchema);

app.post("/signup", (request, response) =>{
	User.findOne({username: request.body.username})
	.then(result => {
		if(result){
			return response.send("Duplicate username found!");
		}else if (request.body.username && request.body.password){
				let newUser = new User({
						username: request.body.username, 
						password: request.body.password
				})
			
			newUser.save().then(save => {
			return response.status(201).send('New user registered')
			}).catch(error => {
				console.error(error);
				response.send("Error saving new user.")
		})

		} else {
			response.send("BOTH username and password must be provided.")
		}	
	})

})


