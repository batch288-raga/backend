// [Section] MongoDB Aggregation
/*
	- used to generate manipulated data and perform opreations to create filtered results that helps analyzing data.
	- compare to doing the CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a frontend application
*/
	// Using aggregate method

	db.fruits.aggregate([
	{ $match: {onSale:true}},
	{ $group: {_id : "$supplier_id", total: {$sum: "$stock"
	}}}
		])



	db.fruits.aggregate([
	{ $match : {onSale: true}}
		])

	// The $group groups the documents in terms of the property declared in the _id property
	db.fruits.aggregate([
		{ $group: {_id : "$supplier_id", total: {$sum: "$stock"
	}}}
		])

	// max operator
	db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group : {_id : "$supplier_id", max : {$max: "$stock"}, sum : {$sum: "$stock"}}}
	])

	db.fruits.aggregate([
	{ $match : { onSale:true}},
	{ $group : {
		_id: "$color",
		max: { $max : "$stock"},
		sum : { $sum : "$stock"}
	}}
	])

// Field projection with aggregation
	/*
		- the $project operater can be used when aggregating data to exclude the returned result
	*/

db.fruits.aggregate([
	{ $match : { onSale:true}},
	{ $group : {
		_id: "$color",
		max: { $max : "$stock"},
		sum : { $sum : "$stock"}
	}},
	{ $project: { _id : 0}}
])

// Sorting aggregated result
	/*
		The $sort operator can be use to change the order of aggregated results
		Providing value of -1 will sort the aggregated  results in a reverse order

		Syntax:
		 - {$sort : {field : 1 / -1}}
	*/
db.fruits.aggregate([
	{ $match: {onSale:true}},
	{ $group : {
		_id : "$supplier_id", 
		total : {$sum : "$stock"}
	}},
	{$sort: {total : 1}}
])

db.fruits.aggregate([
	{ $match: {onSale:true}},
	{ $group : {
		_id : "$name", 
		total : {$sum : "$stock"}
	}},
	{$sort: {_id : 1}}
])