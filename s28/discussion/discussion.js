// show databases - list of the db inside our cluster 
// use 'dbName' - to use a specific database
// show collections - to show the list of collections inside the db.

// CRUD operations
/*
	- CRUD operation is the heart of any backend application
	- mastering the CRUD operation is essential for any developer especially to those who want to become backend developer
*/
// [SECTION] Inserting Document (Create)
	// Insert one document
		/*
			-Since mongoDB deals with objects as it's structure for documents we can easily create them by providing objects in our method/operation.

			Syntax:
				db.collectonName.insertOne({
					object
				})
		*/
db.users.insertOne({
	"firstName" : "Jane",
	"lastName" : "Doe",
	"age" : 21,
	"contact" : {
		"phone" : "123456789",
		"email" : "janedoe@gmail.com",
	},
	"courses" : ["CSS", "JavaScript", "Python"],
	"department" : "none"
})

// Insert Many
/*
	syntax:
		db.collectionName.insertMany([{objectA}, {objectB}];
*/

db.users.insertMany([
	{
		firstName : "Stephen",
		lastName : "Hawking",
		age : 76,
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},

	{
		firstName : "Neil",
		lastName : "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}
])

db.userss.insertOne({
	"firstName" : "Jane",
	"lastName" : "Doe",
	"age" : 21,
	"contact" : {
		"phone" : "123456789",
		"email" : "janedoe@gmail.com",
	},
	"courses" : ["CSS", "JavaScript", "Python"],
	"department" : "none"
})

// [SECTION] Finding documents (Read Operation)
	// db.collectionName.find();
	// db.collectionName.find({field:value});
// Using find() method will show you the list of all the documents inside our collection
db.users.find();

// The "pretty" method allows us to be able to view the documents returned by our terminals to be in a better format
db.users.find().pretty();

// It will return the documents that will pass the "criteria" given in the method
db.users.find({firstName : "Stephen"});

//multiple criteria
db.users.find({lastName : "Armstrong", age:82});

db.users.find({firstName: "Jane"});

db.users.find({cotact: {phone:"123456789"}});


db.users.find({contact: {phone:"123456789", email: "janedoe@gmail.com"}})

db.users.find("contact.phone" : "123456789")

// [SECTION] Updating documents (Update)
db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})

// updateOne method
/*
	Syntax:
		db.collectionName.updateOne({criteria, {$set: {field:value}}});
*/

db.users.updateOne(
	{ firstName: "test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact:{
				phone: "12345678",
				email: "billgates@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "none"
		}
	}
)

db.users.updateOne(
	{ firstName: "Bill"},
	{
		$set: {
			firstName: "Chris"
		}
	}
)

db.users.updateOne(
	{firstName : "Jane"},
	{
		$set: {
			lastName: "Edited"
		}
	}
)

// Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany(
			{criteria},
			{
				$set:{
					{field:value}
				}
			}
		)
*/

db.users.updateMany(
	{ department: "none"},
	{
		$set:{
			department:"HR"
		}
	}
	)

// Replace One
	/*
		Syntax: db.collectionName.replaceOne(
		{criteria}, {
			$set:{
				object
			}
		})
	*/

db.users.insertOne({firstName: "test"});

db.users.replaceOne(
	{ firstName : "test"},
	{
			firstName : "Bill",
			lastName : "Gates",
			age: 65,
			contact: {},
			courses: [],
			department: "Operations"	
	}
	)

// [SECTION] Deleting Documents (Delete)
	/*
		db.collectionName.deleteOne({criteria})
	*/

db.users.deleteOne({firstName : "Bill"});

// Deleting multiple document
	/*
		Syntax: db.collectionName.deleteMany({criteria})
	*/

db.users.deleteMany({firstName : "Jane"});