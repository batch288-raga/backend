// console.log("TGIF!");

// [Section] Exponent Operator;
	// Before the ES6 update 
	// Math.pow(base, exponent);

	// 8 raise to the power of 2
	const firstNum = Math.pow(8, 2);
	console.log(firstNum);

	const secondNum = Math.pow(5, 5);
	console.log(secondNum);


	// After the ES6 update

	const thirdNum = 8 ** 2;
	console.log(thirdNum);

	const fourthNum = 5 ** 2;
	console.log(fourthNum);

// [Section] Template Literals
	// It will allow us to write strings without using the concatenation operator
	// Greatly helps with the code readability.

	// Before ES6 Update
	let name = "John";
	let message = 'Hello ' + name + '! Welcome to programming!'
	console.log(message);

	// After the ES6 Update
	// Uses backticks (``)

	message = `Hello ${name}! Welcome to programming!`;
	console.log(message);

	console.log(typeof message);

	// Multi-line using Template literals
	const anotherMessage = `${name} attended a math competition.
	He won it by solving the problem 8**2 with the 
	solution of ${firstNum}`;
	console.log(anotherMessage);

	const interestRate = .1;
	const principal = 1000;

	// Template literals allows us to write strings with embedded JavaScript expressions
	// expressions are any valid unit of code that resolves to a value
	// "${}" are used to include JavaScript expressions in strings using the template literals
	console.log(`The interest on your savings account is : ${interestRate * principal}`);

// [SECTION] Array Destructuring
	/*
		-allows us to unpack elements in array into distinct variables
		Syntax:
			let/const [variableNameA, variableNameB, variableNameC, ...] = arrayName;
	*/

	const fullName = ["Juan", "Dela", "Cruz"];

	// Before the ES6 Update
	let firstName = fullName[0];
	let middleName = fullName[1];
	let lastName = fullName[2];
	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you`);

	// After the ES6 Update:
		// const [nickName, secondName, familyName] = fullName;
		// console.log(nickName);
		// console.log(secondName);
		// console.log(familyName);

		// nickName = "Pedro";
		// console.log(nickName);

	// Another example of Array Destructuring:
		let gradesPerQuarter = [98, 97, 95, 94];

		let [firstGrading, secondGrading, thirdGrading, fourthGrading ] = gradesPerQuarter;

		// Unlike the "const" keyword, "let" keyword won't cause an error.
		firstGrading = 100;
		console.log(firstGrading);

		console.log(firstGrading);
		console.log(secondGrading);
		console.log(thirdGrading);
		console.log(fourthGrading);

// [Section] Object Destructuring
		// allow us to unpack properties of objects into distinct variables;
		// shortens the syntax for acccessing properties from objects
		/*
			Syntax:
				let/const{ proertyNameA, propertyNameB ... } = objectName;

		*/

	// Before the ES6 update
		const person = {
			givenName: "Jane",
			maidenName: "Dela",
			familyName: "Cruz"
		}

		console.log(person);

		// const givenName = person.givenName;
		// const maidenName = person.maidenName;
		// const familyName = person.familyName;

		// console.log(givenName);
		// console.log(maidenName);
		// console.log(familyName);

	// After the ES6 update

		// The order of the property/variable does not affect its value
		const {maidenName, familyName, givenName} = person;

		console.log(`This is the givenName ${givenName}`);
		console.log(`This is the maidenName ${maidenName}`);
		console.log(`This is the familyName ${familyName}`);

// [SECTION] Arrow function
		// Compact alternative syntax to a traditional functions

		/*
			Syntax:
				const/let variableName = () => {
					statement/codeblock;
				}
		*/
		// Arrow function without parameter
		const hello = () => {
			console.log("Hello World from the arrow function");
		}

		hello();

		// Arrow function with parameter
		/*
			Syntax:
				const/let variableName = (parameter) => {
					statement/codeblock;
				}
		*/

		const printFullName = (firstName, middleInitial, lastName) => {
			console.log(`${firstName} ${middleInitial}. ${lastName}`)
		}

		printFullName('John', 'D', 'Smith');

		// Arrow function can also be used with loops
		// Example
		const students = ["John", "Jane", "Judy"];

		students.forEach((student) => {
			console.log(`${student} is a student.`)
		});

//[SECTION] Implicit Return in Arrow function
		// Example:
			// if the function will run one line or one statement the arrow function will implicitly return the value
			const add = (x,y) => x+y;

			let total = add(10,12);

			console.log(total);

		function trialFunction(x, y){
			return x+y;
		};

		console.log(trialFunction(1,2));

// [SECTION] Default function Argument value
		// provides a default funcction argument value if none is provded when the function is invoked

		const greet = (name = "Hakdog", age = 0) =>{
			return `Good morning, ${name}! I am ${age} years old.`
		}

		console.log(greet('Lloyd', 4));

		function addNumber(x = 0, y = 0){
			console.log(x);
			console.log(y);
			return x + y;
		}

		let sum = addNumber(y=1);
		console.log(sum);

// [SECTION] Class-Based Object Blueprints
		// Allows us to create/instantiate of objects using class as blueprint

		// Syntax:
		/*
			class className{
					construnctor(objectPropertyA, objectPropertyB){
						this.objectPropertyA = objectValueA;
						this.objectPropertyB = objectValueB;
					}
			}

		*/

		class Car {
			constructor(brand, name, year){
				this.brand = brand;
				this.name = name;
				this.year = year;

				this.drive = () => {
					console.log('This car is moving 150 km per hour.')
				}
			};
		}


		// Instantiate an Object

		const myCar = new Car('Ford', 'Ranger Raptor', 2021);

		console.log(myCar);

		const myNewCar = new Car();

		console.log(myNewCar);

		myCar.drive();
		