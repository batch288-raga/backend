let http = require("http");

// mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
]

http.createServer(function(request, response) {	

	if(request.url == "/users" && request.method == "GET") {
		// 'application/json' - sets responst output to JSON data type
		response.writeHead(200, {'Content-Type': 'application/json'});
		// response.write() - method in Node.js that is used to write data the the response body in a HTTP server
		// JSON.stringify() - method converts the string input to JSON
		response.write(JSON.stringify(directory));
		response.end();
	};

	// POST Method
	if(request.url == "/addUser" && request.method == "POST"){
		
		// Declare and initialize a "requestBody" variable to an empty string
		let requestBody = "";

		// request.on() - event listener in Node.js that is use to handle incoming data in HTTP server
		// data - is received from the client and is processed in the "data" stream
		request.on('data', function(data){
			// Assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		});

		// response end step - only runs after the request has completely been sent.
		request.on('end', function(){
			// Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			}

			// Add the new user 
			directory.push(newUser)
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.end(JSON.stringify(newUser))
		});
	}
}).listen(5000)

console.log('Server running at localhost: 5000');