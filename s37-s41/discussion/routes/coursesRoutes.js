const express = require("express");

const coursesControllers = require("../controllers/coursesControllers");

const auth = require("../auth.js");

const router = express.Router();

// This route is responsible for adding course in our db.
router.post("/addCourse", auth.verify, coursesControllers.addCourse);

// Route for retrieving all courses
router.get("/", auth.verify, coursesControllers.getAllCourses);

// Route for retrieving all courses
router.get('/activeCourses', coursesControllers.getActiveCourses);

// Route for inactive courses
router.get('/inactiveCourses', auth.verify, coursesControllers.getInactiveCourse)

// Route for getting the specific course information
router.get('/:courseId', coursesControllers.getCourse);

// Route for updating course
router.patch('/:courseId', auth.verify, coursesControllers.updateCourse);

// Route for archiving course
router.patch('/:courseId/archive', auth.verify,coursesControllers.archiveCourse);



module.exports = router;
