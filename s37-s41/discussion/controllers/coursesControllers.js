const Courses = require("../models/Courses.js");

const bcrypt = require("bcrypt");

const auth = require("../auth.js");

// This controller is for the course creation
module.exports.addCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	// console.log(userData);

	if(userData.isAdmin){

		// Create an object using the Courses Model
		const newCourse = new Courses({
			// supply all the required field declared on the courses model
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			isActive: request.body.isActive,
			slots: request.body.slots
		})

		newCourse.save()
		.then(save => response.send(true))
		.catch(error => response.send(false));

	}else{
		return response.send(false)
	}

}

// In this controller we are going to retrieve all of the course in our database
module.exports.getAllCourses = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){

		Courses.find({})
		.then(result => response.send(result))
		.catch(error => response.send(false))

	}else{
		return response.send(false)
	}
}

// Route for retrieving all active courses
module.exports.getActiveCourses = (request, response) => {

	Courses.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

// Controller will retrieve the information of a single document using the provided params, 
module.exports.getCourse = (request, response) => {

	const courseId = request.params.courseId;

	Courses.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(false))

}

// this controller will update our document or course
module.exports.updateCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	//description, price and name

	let updatedCourse = {
		name : request.body.name,
		description : request.body.description,
		price : request.body.price
	}

	if(userData.isAdmin){

		Courses.findByIdAndUpdate(courseId, updatedCourse)
		.then(result => response.send(true))
		.catch(error => response.send(false))
	}else{
		return response.send(false)
	}
}

// this controller will archive the course using patch
module.exports.archiveCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	if(userData.isAdmin && courseId){
		Courses.findByIdAndUpdate(courseId, {isActive:request.body.isActive})
		.then(result => {
			if(result){
				response.send(true)
			}else{
				response.send(true)
			}
		}).catch(error => response.send(false))
	}else{
		return response.send(false)
	}
}

// retrieving in archived courses
module.exports.getInactiveCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send(false)
	}else{
		Courses.find({isActive : false})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
}